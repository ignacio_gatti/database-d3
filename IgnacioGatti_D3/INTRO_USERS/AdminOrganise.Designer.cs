﻿namespace INTRO_USERS
{
    partial class AdminOrganise
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DaysWorking1 = new System.Windows.Forms.ComboBox();
            this.CarsList1 = new System.Windows.Forms.ComboBox();
            this.InstructorList1 = new System.Windows.Forms.ComboBox();
            this.InstructorList2 = new System.Windows.Forms.ComboBox();
            this.InstructorList3 = new System.Windows.Forms.ComboBox();
            this.DaysWorking3 = new System.Windows.Forms.ComboBox();
            this.DaysWorking2 = new System.Windows.Forms.ComboBox();
            this.CarsList3 = new System.Windows.Forms.ComboBox();
            this.CarsList2 = new System.Windows.Forms.ComboBox();
            this.LoginPage = new System.Windows.Forms.Button();
            this.InstructorsAssignedHoursList = new System.Windows.Forms.ListBox();
            this.AssignInstructorHours1 = new System.Windows.Forms.Button();
            this.AssignInstructorHours3 = new System.Windows.Forms.Button();
            this.AssignInstructorHours2 = new System.Windows.Forms.Button();
            this.ShowOnlyList = new System.Windows.Forms.ComboBox();
            this.ShowOnlyBtn = new System.Windows.Forms.Button();
            this.ShowEveryone = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.staffList = new System.Windows.Forms.ComboBox();
            this.removeStaff = new System.Windows.Forms.Button();
            this.showStatistics = new System.Windows.Forms.Button();
            this.ClientAppointmentComplete = new System.Windows.Forms.ListBox();
            this.sendBill = new System.Windows.Forms.Button();
            this.clientPaid = new System.Windows.Forms.Button();
            this.RemoveCarAppointment = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // DaysWorking1
            // 
            this.DaysWorking1.FormattingEnabled = true;
            this.DaysWorking1.Location = new System.Drawing.Point(140, 39);
            this.DaysWorking1.Name = "DaysWorking1";
            this.DaysWorking1.Size = new System.Drawing.Size(139, 21);
            this.DaysWorking1.TabIndex = 1;
            // 
            // CarsList1
            // 
            this.CarsList1.FormattingEnabled = true;
            this.CarsList1.Location = new System.Drawing.Point(285, 39);
            this.CarsList1.Name = "CarsList1";
            this.CarsList1.Size = new System.Drawing.Size(121, 21);
            this.CarsList1.TabIndex = 2;
            // 
            // InstructorList1
            // 
            this.InstructorList1.FormattingEnabled = true;
            this.InstructorList1.Location = new System.Drawing.Point(13, 39);
            this.InstructorList1.Name = "InstructorList1";
            this.InstructorList1.Size = new System.Drawing.Size(121, 21);
            this.InstructorList1.TabIndex = 5;
            // 
            // InstructorList2
            // 
            this.InstructorList2.FormattingEnabled = true;
            this.InstructorList2.Location = new System.Drawing.Point(13, 105);
            this.InstructorList2.Name = "InstructorList2";
            this.InstructorList2.Size = new System.Drawing.Size(121, 21);
            this.InstructorList2.TabIndex = 6;
            // 
            // InstructorList3
            // 
            this.InstructorList3.FormattingEnabled = true;
            this.InstructorList3.Location = new System.Drawing.Point(13, 179);
            this.InstructorList3.Name = "InstructorList3";
            this.InstructorList3.Size = new System.Drawing.Size(121, 21);
            this.InstructorList3.TabIndex = 7;
            // 
            // DaysWorking3
            // 
            this.DaysWorking3.FormattingEnabled = true;
            this.DaysWorking3.Location = new System.Drawing.Point(140, 179);
            this.DaysWorking3.Name = "DaysWorking3";
            this.DaysWorking3.Size = new System.Drawing.Size(139, 21);
            this.DaysWorking3.TabIndex = 8;
            // 
            // DaysWorking2
            // 
            this.DaysWorking2.FormattingEnabled = true;
            this.DaysWorking2.Location = new System.Drawing.Point(140, 105);
            this.DaysWorking2.Name = "DaysWorking2";
            this.DaysWorking2.Size = new System.Drawing.Size(139, 21);
            this.DaysWorking2.TabIndex = 9;
            // 
            // CarsList3
            // 
            this.CarsList3.FormattingEnabled = true;
            this.CarsList3.Location = new System.Drawing.Point(285, 179);
            this.CarsList3.Name = "CarsList3";
            this.CarsList3.Size = new System.Drawing.Size(121, 21);
            this.CarsList3.TabIndex = 11;
            // 
            // CarsList2
            // 
            this.CarsList2.FormattingEnabled = true;
            this.CarsList2.Location = new System.Drawing.Point(285, 105);
            this.CarsList2.Name = "CarsList2";
            this.CarsList2.Size = new System.Drawing.Size(121, 21);
            this.CarsList2.TabIndex = 12;
            // 
            // LoginPage
            // 
            this.LoginPage.Location = new System.Drawing.Point(1311, 604);
            this.LoginPage.Name = "LoginPage";
            this.LoginPage.Size = new System.Drawing.Size(117, 23);
            this.LoginPage.TabIndex = 13;
            this.LoginPage.Text = "Log In Page";
            this.LoginPage.UseVisualStyleBackColor = true;
            this.LoginPage.Click += new System.EventHandler(this.LoginPage_Click);
            // 
            // InstructorsAssignedHoursList
            // 
            this.InstructorsAssignedHoursList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.InstructorsAssignedHoursList.FormattingEnabled = true;
            this.InstructorsAssignedHoursList.ItemHeight = 15;
            this.InstructorsAssignedHoursList.Location = new System.Drawing.Point(510, 39);
            this.InstructorsAssignedHoursList.Name = "InstructorsAssignedHoursList";
            this.InstructorsAssignedHoursList.Size = new System.Drawing.Size(901, 169);
            this.InstructorsAssignedHoursList.TabIndex = 14;
            // 
            // AssignInstructorHours1
            // 
            this.AssignInstructorHours1.Location = new System.Drawing.Point(412, 37);
            this.AssignInstructorHours1.Name = "AssignInstructorHours1";
            this.AssignInstructorHours1.Size = new System.Drawing.Size(75, 23);
            this.AssignInstructorHours1.TabIndex = 15;
            this.AssignInstructorHours1.Text = "Update";
            this.AssignInstructorHours1.UseVisualStyleBackColor = true;
            this.AssignInstructorHours1.Click += new System.EventHandler(this.AssignInstructorHours1_Click);
            // 
            // AssignInstructorHours3
            // 
            this.AssignInstructorHours3.Location = new System.Drawing.Point(412, 177);
            this.AssignInstructorHours3.Name = "AssignInstructorHours3";
            this.AssignInstructorHours3.Size = new System.Drawing.Size(75, 23);
            this.AssignInstructorHours3.TabIndex = 16;
            this.AssignInstructorHours3.Text = "Update";
            this.AssignInstructorHours3.UseVisualStyleBackColor = true;
            this.AssignInstructorHours3.Click += new System.EventHandler(this.AssignInstructorHours3_Click);
            // 
            // AssignInstructorHours2
            // 
            this.AssignInstructorHours2.Location = new System.Drawing.Point(412, 105);
            this.AssignInstructorHours2.Name = "AssignInstructorHours2";
            this.AssignInstructorHours2.Size = new System.Drawing.Size(75, 23);
            this.AssignInstructorHours2.TabIndex = 17;
            this.AssignInstructorHours2.Text = "Update";
            this.AssignInstructorHours2.UseVisualStyleBackColor = true;
            this.AssignInstructorHours2.Click += new System.EventHandler(this.AssignInstructorHours2_Click);
            // 
            // ShowOnlyList
            // 
            this.ShowOnlyList.FormattingEnabled = true;
            this.ShowOnlyList.Location = new System.Drawing.Point(510, 237);
            this.ShowOnlyList.Name = "ShowOnlyList";
            this.ShowOnlyList.Size = new System.Drawing.Size(121, 21);
            this.ShowOnlyList.TabIndex = 19;
            // 
            // ShowOnlyBtn
            // 
            this.ShowOnlyBtn.Location = new System.Drawing.Point(649, 237);
            this.ShowOnlyBtn.Name = "ShowOnlyBtn";
            this.ShowOnlyBtn.Size = new System.Drawing.Size(172, 23);
            this.ShowOnlyBtn.TabIndex = 20;
            this.ShowOnlyBtn.Text = "Show Only This Instructor";
            this.ShowOnlyBtn.UseVisualStyleBackColor = true;
            this.ShowOnlyBtn.Click += new System.EventHandler(this.ShowOnlyBtn_Click);
            // 
            // ShowEveryone
            // 
            this.ShowEveryone.Location = new System.Drawing.Point(842, 237);
            this.ShowEveryone.Name = "ShowEveryone";
            this.ShowEveryone.Size = new System.Drawing.Size(111, 23);
            this.ShowEveryone.TabIndex = 21;
            this.ShowEveryone.Text = "Show Everyone";
            this.ShowEveryone.UseVisualStyleBackColor = true;
            this.ShowEveryone.Click += new System.EventHandler(this.ShowEveryone_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label1.Location = new System.Drawing.Point(1291, 268);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 24);
            this.label1.TabIndex = 22;
            this.label1.Text = "Remove Staff";
            // 
            // staffList
            // 
            this.staffList.FormattingEnabled = true;
            this.staffList.Location = new System.Drawing.Point(1295, 306);
            this.staffList.Name = "staffList";
            this.staffList.Size = new System.Drawing.Size(117, 21);
            this.staffList.TabIndex = 23;
            this.staffList.SelectedIndexChanged += new System.EventHandler(this.staffList_SelectedIndexChanged);
            // 
            // removeStaff
            // 
            this.removeStaff.Location = new System.Drawing.Point(1295, 348);
            this.removeStaff.Name = "removeStaff";
            this.removeStaff.Size = new System.Drawing.Size(129, 23);
            this.removeStaff.TabIndex = 24;
            this.removeStaff.Text = "Remove:";
            this.removeStaff.UseVisualStyleBackColor = true;
            this.removeStaff.Click += new System.EventHandler(this.removeStaff_Click);
            // 
            // showStatistics
            // 
            this.showStatistics.Location = new System.Drawing.Point(1307, 518);
            this.showStatistics.Name = "showStatistics";
            this.showStatistics.Size = new System.Drawing.Size(117, 23);
            this.showStatistics.TabIndex = 25;
            this.showStatistics.Text = "Show Statistics";
            this.showStatistics.UseVisualStyleBackColor = true;
            this.showStatistics.Click += new System.EventHandler(this.showStatistics_Click);
            // 
            // ClientAppointmentComplete
            // 
            this.ClientAppointmentComplete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.ClientAppointmentComplete.FormattingEnabled = true;
            this.ClientAppointmentComplete.ItemHeight = 16;
            this.ClientAppointmentComplete.Location = new System.Drawing.Point(12, 306);
            this.ClientAppointmentComplete.Name = "ClientAppointmentComplete";
            this.ClientAppointmentComplete.Size = new System.Drawing.Size(1229, 292);
            this.ClientAppointmentComplete.TabIndex = 26;
            // 
            // sendBill
            // 
            this.sendBill.Location = new System.Drawing.Point(13, 604);
            this.sendBill.Name = "sendBill";
            this.sendBill.Size = new System.Drawing.Size(179, 23);
            this.sendBill.TabIndex = 27;
            this.sendBill.Text = "Send Bill";
            this.sendBill.UseVisualStyleBackColor = true;
            this.sendBill.Click += new System.EventHandler(this.sendBill_Click);
            // 
            // clientPaid
            // 
            this.clientPaid.Location = new System.Drawing.Point(217, 605);
            this.clientPaid.Name = "clientPaid";
            this.clientPaid.Size = new System.Drawing.Size(189, 23);
            this.clientPaid.TabIndex = 28;
            this.clientPaid.Text = "Client Paid";
            this.clientPaid.UseVisualStyleBackColor = true;
            this.clientPaid.Click += new System.EventHandler(this.clientPaid_Click);
            // 
            // RemoveCarAppointment
            // 
            this.RemoveCarAppointment.Location = new System.Drawing.Point(980, 236);
            this.RemoveCarAppointment.Name = "RemoveCarAppointment";
            this.RemoveCarAppointment.Size = new System.Drawing.Size(131, 23);
            this.RemoveCarAppointment.TabIndex = 29;
            this.RemoveCarAppointment.Text = "Remove";
            this.RemoveCarAppointment.UseVisualStyleBackColor = true;
            this.RemoveCarAppointment.Click += new System.EventHandler(this.RemoveCarAppointment_Click);
            // 
            // AdminOrganise
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1484, 657);
            this.Controls.Add(this.RemoveCarAppointment);
            this.Controls.Add(this.clientPaid);
            this.Controls.Add(this.sendBill);
            this.Controls.Add(this.ClientAppointmentComplete);
            this.Controls.Add(this.showStatistics);
            this.Controls.Add(this.removeStaff);
            this.Controls.Add(this.staffList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ShowEveryone);
            this.Controls.Add(this.ShowOnlyBtn);
            this.Controls.Add(this.ShowOnlyList);
            this.Controls.Add(this.AssignInstructorHours2);
            this.Controls.Add(this.AssignInstructorHours3);
            this.Controls.Add(this.AssignInstructorHours1);
            this.Controls.Add(this.InstructorsAssignedHoursList);
            this.Controls.Add(this.LoginPage);
            this.Controls.Add(this.CarsList2);
            this.Controls.Add(this.CarsList3);
            this.Controls.Add(this.DaysWorking2);
            this.Controls.Add(this.DaysWorking3);
            this.Controls.Add(this.InstructorList3);
            this.Controls.Add(this.InstructorList2);
            this.Controls.Add(this.InstructorList1);
            this.Controls.Add(this.CarsList1);
            this.Controls.Add(this.DaysWorking1);
            this.Name = "AdminOrganise";
            this.Text = "AdminOrganise";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox DaysWorking1;
        private System.Windows.Forms.ComboBox CarsList1;
        private System.Windows.Forms.ComboBox InstructorList1;
        private System.Windows.Forms.ComboBox InstructorList2;
        private System.Windows.Forms.ComboBox InstructorList3;
        private System.Windows.Forms.ComboBox DaysWorking3;
        private System.Windows.Forms.ComboBox DaysWorking2;
        private System.Windows.Forms.ComboBox CarsList3;
        private System.Windows.Forms.ComboBox CarsList2;
        private System.Windows.Forms.Button LoginPage;
        private System.Windows.Forms.ListBox InstructorsAssignedHoursList;
        private System.Windows.Forms.Button AssignInstructorHours1;
        private System.Windows.Forms.Button AssignInstructorHours3;
        private System.Windows.Forms.Button AssignInstructorHours2;
        private System.Windows.Forms.ComboBox ShowOnlyList;
        private System.Windows.Forms.Button ShowOnlyBtn;
        private System.Windows.Forms.Button ShowEveryone;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox staffList;
        private System.Windows.Forms.Button removeStaff;
        private System.Windows.Forms.Button showStatistics;
        private System.Windows.Forms.ListBox ClientAppointmentComplete;
        private System.Windows.Forms.Button sendBill;
        private System.Windows.Forms.Button clientPaid;
        private System.Windows.Forms.Button RemoveCarAppointment;
    }
}