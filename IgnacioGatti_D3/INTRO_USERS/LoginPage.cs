﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    //Name:
    //Student ID:

    public partial class LoginPage : Form
    {
        string NextPage = "";
        public static SqlConnection con = new SqlConnection(@"Data Source=localhost\;Database=D2_9935621;Integrated Security=True");
        string loggedOn = "";
        public LoginPage()
        {
            InitializeComponent();
            //This line of code allows us to obscure the password visually and limit the max chars in textbox
            textBoxPassword.PasswordChar = '*';     //password character to hide password characters
            textBoxPassword.MaxLength = 20;         //max textbox character count
        }


        
        private void buttonLogin_Click(object sender, EventArgs e)
        {
            //Variables to be used: 1x bool, 4x string
            
            string username = "", firstname = "", lastname = "", password = "", competence = "";

            //check if boxes are empty, the Trim removes white space in text from either side
            if ("".Equals(textBoxUserName.Text.Trim()) || "".Equals(textBoxPassword.Text.Trim()))
            {
                MessageBox.Show("Please make sure you enter a Username and Password");
                return;
            }

            //(1) GET the username and password from the text boxes, is good to put them in a try catch
            try
            {
                /*YOUR CODE HERE*/
                username = textBoxUserName.Text.Trim();
                password = textBoxPassword.Text.Trim();
            }
            catch
            {
                //Error message, more useful when you are storing numbers etc. into the database.
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }

            
            SQL.selectQuery("SELECT * FROM clients");
            
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (username.Equals(SQL.read[1].ToString()) && password.Equals(SQL.read[2].ToString()))
                    {
                        loggedOn = "clientLoggedIn";
                        firstname = SQL.read[4].ToString();
                        lastname = SQL.read[3].ToString();
                        competence = SQL.read[7].ToString();
                        break;
                    }
                }
            }

            
            else
            {
                
                MessageBox.Show("Login attempt unsuccessful! Please check details");
                textBoxUserName.Focus();
                return;
            }

            SQL.selectQuery("SELECT * FROM admin");
            
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (username.Equals(SQL.read[1].ToString()) && password.Equals(SQL.read[2].ToString()))
                    {
                        loggedOn = "adminLoggedIn";
                        firstname = SQL.read[4].ToString();
                        lastname = SQL.read[3].ToString();
                        break;
                    }
                }
            }

            
            else
            {
                
                MessageBox.Show("Login attempt unsuccessful! Please check details");
                textBoxUserName.Focus();
                return;
            }

            SQL.selectQuery("SELECT * FROM instructor");
            
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (username.Equals(SQL.read[1].ToString()) && password.Equals(SQL.read[2].ToString()))
                    {
                        loggedOn = "instructorLoggedIn";
                        firstname = SQL.read[4].ToString();
                        lastname = SQL.read[3].ToString();
                        break;
                    }
                }
            }

            // ************ GO TO CLIENT PAGE ****************

            if (loggedOn == "clientLoggedIn")
            {
                
                MessageBox.Show("Successfully logged in as: " + firstname + " " + lastname);
                initialiseTextBoxes();


                //Hides the login page form from user
                this.Hide();
                
                //Create a Register Page object to change to
                ClientScheduleAppointment register = new ClientScheduleAppointment(username, competence);
                //show the register page
                register.ShowDialog();
                //close the login page we are currently on
                this.Close();
            }

            // ****************** GO TO INSTRUCTOR PAGE *******************
            else if(loggedOn== "instructorLoggedIn")
            {
                MessageBox.Show("Successfully logged in as: " + firstname + " " + lastname);
                initialiseTextBoxes();

                //Hides the login page form from user
                this.Hide();
                //Create a Register Page object to change to
                InstructorAvailability register = new InstructorAvailability(username, firstname);
                //show the register page
                register.ShowDialog();
                //close the login page we are currently on
                this.Close();
            }

            else if (loggedOn == "adminLoggedIn")
            {
                MessageBox.Show("Successfully logged in as: " + firstname + " " + lastname);
                initialiseTextBoxes();

                //Hides the login page form from user
                this.Hide();
                //Create a Register Page object to change to
                AdminOrganise register = new AdminOrganise();
                //show the register page
                register.ShowDialog();
                //close the login page we are currently on
                this.Close();
            }
            else
            {
                
                MessageBox.Show("Login attempt unsuccessful! Please check details");
                textBoxUserName.Focus();
                return;
            }


        }


        
        private void labelRegister_Click(object sender, EventArgs e)
        {
            //Hides the login page form from user
            this.Hide();
            //Create a Register Page object to change to
            RegisterPage register = new RegisterPage();
            //show the register page
            register.ShowDialog();
            //close the login page we are currently on
            this.Close();
        }

        private void RegisterAdminLbl_Click(object sender, EventArgs e)
        {
            NextPage = "RegisterAdmin";

            //Hides the login page form from user
            this.Hide();
            //Create a Register Page object to change to
            VerifyPage register = new VerifyPage(NextPage);
            //show the register page
            register.ShowDialog();
            //close the login page we are currently on
            this.Close();
        }

        private void RegisterInstLbl_Click(object sender, EventArgs e)
        {
            NextPage = "RegisterInst";

            //Hides the login page form from user
            this.Hide();
            //Create a Register Page object to change to
            VerifyPage register = new VerifyPage(NextPage);
            //show the register page
            register.ShowDialog();
            //close the login page we are currently on
            this.Close();
        }


        private void buttonClearAll_Click(object sender, EventArgs e)
        {
            initialiseTextBoxes();
        }

        
        public void initialiseTextBoxes()
        {
            //goes through and clears all of the textboxes
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    (c as TextBox).Clear();
                }
            }
            //makes next place user types the text box
            textBoxUserName.Focus();
        }

        
        private void labelRegister_MouseHover(object sender, EventArgs e)
        {
            //Changes the colour of the label
            labelRegister.ForeColor = Color.Blue;
        }

        private void labelRegister_MouseLeave(object sender, EventArgs e)
        {
            //Changes the colour of the label
            labelRegister.ForeColor = Color.Black;
        }

        
        
    }
}
