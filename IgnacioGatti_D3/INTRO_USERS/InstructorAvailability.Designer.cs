﻿namespace INTRO_USERS
{
    partial class InstructorAvailability
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.InstructorUsernameLabel = new System.Windows.Forms.Label();
            this.BookedAppointmentList = new System.Windows.Forms.ListBox();
            this.ReturnLogInPage = new System.Windows.Forms.Button();
            this.confirmAppointment = new System.Windows.Forms.Button();
            this.ConfirmedAppointment = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.completeAppointment = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // InstructorUsernameLabel
            // 
            this.InstructorUsernameLabel.AutoSize = true;
            this.InstructorUsernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InstructorUsernameLabel.Location = new System.Drawing.Point(428, 9);
            this.InstructorUsernameLabel.Name = "InstructorUsernameLabel";
            this.InstructorUsernameLabel.Size = new System.Drawing.Size(83, 20);
            this.InstructorUsernameLabel.TabIndex = 33;
            this.InstructorUsernameLabel.Text = "Username";
            this.InstructorUsernameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BookedAppointmentList
            // 
            this.BookedAppointmentList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.BookedAppointmentList.FormattingEnabled = true;
            this.BookedAppointmentList.ItemHeight = 16;
            this.BookedAppointmentList.Location = new System.Drawing.Point(27, 49);
            this.BookedAppointmentList.Name = "BookedAppointmentList";
            this.BookedAppointmentList.Size = new System.Drawing.Size(917, 196);
            this.BookedAppointmentList.TabIndex = 35;
            this.BookedAppointmentList.SelectedIndexChanged += new System.EventHandler(this.BookedAppointmentList_SelectedIndexChanged);
            // 
            // ReturnLogInPage
            // 
            this.ReturnLogInPage.Location = new System.Drawing.Point(27, 497);
            this.ReturnLogInPage.Name = "ReturnLogInPage";
            this.ReturnLogInPage.Size = new System.Drawing.Size(262, 23);
            this.ReturnLogInPage.TabIndex = 36;
            this.ReturnLogInPage.Text = "Log In Page";
            this.ReturnLogInPage.UseVisualStyleBackColor = true;
            this.ReturnLogInPage.Click += new System.EventHandler(this.ReturnLogInPage_Click);
            // 
            // confirmAppointment
            // 
            this.confirmAppointment.Location = new System.Drawing.Point(27, 318);
            this.confirmAppointment.Name = "confirmAppointment";
            this.confirmAppointment.Size = new System.Drawing.Size(262, 29);
            this.confirmAppointment.TabIndex = 39;
            this.confirmAppointment.Text = "Confirm Appointment With:";
            this.confirmAppointment.UseVisualStyleBackColor = true;
            this.confirmAppointment.Click += new System.EventHandler(this.confirmAppointment_Click);
            // 
            // ConfirmedAppointment
            // 
            this.ConfirmedAppointment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ConfirmedAppointment.FormattingEnabled = true;
            this.ConfirmedAppointment.Location = new System.Drawing.Point(295, 318);
            this.ConfirmedAppointment.Name = "ConfirmedAppointment";
            this.ConfirmedAppointment.Size = new System.Drawing.Size(649, 212);
            this.ConfirmedAppointment.TabIndex = 40;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.label1.Location = new System.Drawing.Point(510, 279);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(193, 20);
            this.label1.TabIndex = 41;
            this.label1.Text = "Confirmed Appointments";
            // 
            // completeAppointment
            // 
            this.completeAppointment.Location = new System.Drawing.Point(27, 398);
            this.completeAppointment.Name = "completeAppointment";
            this.completeAppointment.Size = new System.Drawing.Size(262, 23);
            this.completeAppointment.TabIndex = 42;
            this.completeAppointment.Text = "Complete Appointment";
            this.completeAppointment.UseVisualStyleBackColor = true;
            this.completeAppointment.Click += new System.EventHandler(this.completeAppointment_Click);
            // 
            // InstructorAvailability
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(956, 566);
            this.Controls.Add(this.completeAppointment);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ConfirmedAppointment);
            this.Controls.Add(this.confirmAppointment);
            this.Controls.Add(this.ReturnLogInPage);
            this.Controls.Add(this.BookedAppointmentList);
            this.Controls.Add(this.InstructorUsernameLabel);
            this.Name = "InstructorAvailability";
            this.Text = "InstructorAvailability";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.Label InstructorUsernameLabel;
        private System.Windows.Forms.ListBox BookedAppointmentList;
        private System.Windows.Forms.Button ReturnLogInPage;
        private System.Windows.Forms.Button confirmAppointment;
        private System.Windows.Forms.ListBox ConfirmedAppointment;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button completeAppointment;
    }
}