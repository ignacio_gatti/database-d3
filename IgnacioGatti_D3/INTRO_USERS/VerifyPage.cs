﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class VerifyPage : Form
    {
        public string nxtPg = "";
        public VerifyPage(string NextPage)
        {
            InitializeComponent();

            //Getting 'NextPage' from 'LoginPage.cs' and assigning it to nxtPg 
            //So that the user will go to the Register Page they specified in 'LoginPage.cs'
            if (NextPage == "RegisterAdmin")
            {
                nxtPg = "RegisterAdmin";
            }

            else if (NextPage == "RegisterInst")
            {
                nxtPg = "RegisterInst";
            }

            textBoxPassword.PasswordChar = '*';     //password character to hide password characters
            textBoxPassword.MaxLength = 20;
        }   

        private void VerifyUnPw_Click(object sender, EventArgs e)
        {
            
            string username = "", firstname = "", lastname = "", password = "", loggedOn = "";
            //check if boxes are empty, the Trim removes white space in text from either side
            if ("".Equals(textBoxUserName.Text.Trim()) || "".Equals(textBoxPassword.Text.Trim()))
            {
                MessageBox.Show("Please make sure you enter a Username and Password");
                return;
            }

                        
            try
            {
                /*YOUR CODE HERE*/
                username = textBoxUserName.Text.Trim();
                password = textBoxPassword.Text.Trim();
            }
            catch
            {
                //Error message, more useful when you are storing numbers etc. into the database.
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }

            SQL.selectQuery("SELECT * FROM admin");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (username.Equals(SQL.read[1].ToString()) && password.Equals(SQL.read[2].ToString()))
                    {
                        loggedOn = "adminLoggedIn";
                        firstname = SQL.read[4].ToString();
                        lastname = SQL.read[3].ToString();
                        break;

                    }
                }
            }

            else
            {

                MessageBox.Show("Login attempt unsuccessful! Please check details");
                textBoxUserName.Focus();
                return;
            }


            if (nxtPg == "RegisterAdmin" && loggedOn == "adminLoggedIn")
            {
                //Hides the login page form from user
                this.Hide();
                //Create a Register Page object to change to
                RegisterAdmin register = new RegisterAdmin();
                //show the register page
                register.ShowDialog();
                //close the login page we are currently on
                this.Close();
            }

            else if (nxtPg == "RegisterInst" && loggedOn == "adminLoggedIn")
            {
                //Hides the login page form from user
                this.Hide();
                //Create a Register Page object to change to
                RegisterInst register = new RegisterInst();
                //show the register page
                register.ShowDialog();
                //close the login page we are currently on
                this.Close();
            }

            
        }

        private void LogIn_Click(object sender, EventArgs e)
        {
            //hides this form currently on
            Hide();
            //is the login page as a new object               
            LoginPage login = new LoginPage();
            //Shows the login page window
            login.ShowDialog();
            //closes the current open windows so its only the new one showing
            this.Close();
        }
    }
}
