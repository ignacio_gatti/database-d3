
USE master
GO
drop database D2_9935621
GO
CREATE DATABASE D2_9935621
GO
USE D2_9935621
GO


CREATE TABLE clients(
cid INT NOT NULL IDENTITY(1,1),
client_username varchar (30) primary key not null,
password varchar (30) not null,
sname varchar (30) not null,
fname varchar (30) not null,
email varchar (30) not null,
phone varchar (30) not null,
competence varchar(30) not null,
Check (email like '%@%.%')
)
--SELECT * FROM clients
--DROP TABLE clients

INSERT INTO clients VALUES('a','a','a','a','a@gmail.com', '555-555','Beginner Level')
INSERT INTO clients VALUES('b','b','b','b','b@gmail.com', '555-555','Beginner Level')

CREATE TABLE instructor(
iid INT NOT NULL IDENTITY(1,1) ,
instructor_username varchar (30) primary key not null,
password varchar (30) not null,
sname varchar (30) not null,
fname varchar (30) not null,
email varchar (30) not null,
phone varchar (30) not null,
Check (email like '%@%.%')

)

--SELECT * FROM instructor
--DROP TABLE instructor


CREATE TABLE admin(
aid INT NOT NULL IDENTITY(1,1),
username varchar(30) primary key not null,
password varchar(30) not null,
sname varchar(30) not null,
fname varchar(30) not null,
email varchar(30) not null,
Check (email like '%@%.%')
)
--DROP TABLE admin

CREATE TABLE type(
name varchar (30) primary key not null,
cost float not null,
hours int not null
)


CREATE TABLE document(
id int primary key identity (1,1) not null ,
date int not null,
link varchar(60) not null,
type varchar(30) not null,
check (type like 'bill' OR type like 'document')

)

CREATE TABLE timeslot(
tid int not null,
time varchar(30) not null,
date VARCHAR(30) not null,

primary key (tid, time, date)

)

--DROP TABLE timeslot

CREATE TABLE cars(
license varchar(30) primary key not null,
make varchar(30) not null
)



CREATE TABLE appointment(
id int not null IDENTITY(1,1),
time varchar(50) not  null,
date VARCHAR(50) not null,

client_username varchar(30) not null,
competence VARCHAR(30) not null,
instructor_username varchar(30) not null,
seen VARCHAR (30) not null,
primary key(id)

)

SELECT * FROM appointment app, type tp WHERE seen = 'Completed' AND app.competence = tp.name
--DROP TABLE appointment
--SELECT * FROM appointment

INSERT INTO appointment VALUES ('7:00-8:00','2017-01-2  MONDAY','a', 'Beginner Level', 'Brad','not yet' )
INSERT INTO appointment VALUES ('7:00-8:00','2017-01-3  MONDAY','a', 'Beginner Level', 'Brad','Confirmed' )
INSERT INTO appointment VALUES ('7:00-8:00','2017-01-4  MONDAY','b', 'Experienced Level', 'Brad','Completed' )




CREATE TABLE workingHours(
wid INT NOT NULL IDENTITY(1,1),
instructor_username varchar(30),
dayOfWeek varchar(30)  null,
car varchar(30) not null

)

--SELECT * FROM workingHours

--DROP TABLE workingHours
--DELETE FROM workingHours
INSERT INTO workingHours VALUES('bradp','MONDAY - 13 Hrs','V4FFER - Toyota')
INSERT INTO workingHours VALUES('bradp','TUESDAY - 13 Hrs','HURY25 - Renault')



INSERT INTO cars VALUES('V4FFER', 'Toyota')
INSERT INTO cars VALUES('HURY25', 'Renault')
INSERT INTO cars VALUES('598KHJ', 'Toyota')
INSERT INTO cars VALUES('PPIL88', 'Volkswagen')
INSERT INTO cars VALUES('LL99362', 'Renault')

--DROP TABLE type
INSERT INTO type VALUES('Beginner Level',39.99,5)
INSERT INTO type VALUES('Experienced Level',19.99,2)


INSERT INTO instructor Values ('johnv', 'johnpw', 'Vox', 'John', 'johnv@gmail.com', '555-555')
INSERT INTO instructor Values ('maxl', 'maxpw', 'Landis', 'Max', 'maxl@outlook.com', '555-888')
INSERT INTO instructor Values ('bradp', 'bradpw', 'Pitt', 'Brad', 'bradp@gmail.com', '557-082')
INSERT INTO instructor Values ('mattd', 'mattpw', 'Damon', 'Matt', 'mattd@gmail.com', '588-666')


INSERT INTO admin VALUES('jackj', 'jackpw','Johnson','Jack','jackj@gmail.com')
INSERT INTO admin VALUES('katyp', 'katypw','Perry','Katy','katyp@outlook.com')
INSERT INTO admin VALUES('bobm', 'bobpw','Marley','Bob','bobm@gmail.com')





--DO WHILE loop to automatically fill in the timeslot days and hours 
DECLARE @day INT
SET @day = 1
WHILE (@day <= 30)
BEGIN
		
		DECLARE @hour INT
		DECLARE @endOfHour INT
		SET @hour = 7
		SET @endOfHour = 8
		
	WHILE (@hour <= 19) 
	BEGIN
		
		
		
		INSERT INTO timeslot VALUES (1, (SELECT CONVERT(varchar(30), @hour)) + ':00 -' + (SELECT CONVERT(varchar(30), @endOfHour)) + ':00' ,'2017-01-' + (SELECT CONVERT(varchar(30), @day)))
		INSERT INTO timeslot VALUES (2, (SELECT CONVERT(varchar(30), @hour)) + ':00 -' + (SELECT CONVERT(varchar(30), @endOfHour)) + ':00' ,'2017-01-' + (SELECT CONVERT(varchar(30), @day)))
		INSERT INTO timeslot VALUES (3, (SELECT CONVERT(varchar(30), @hour)) + ':00 -' + (SELECT CONVERT(varchar(30), @endOfHour)) + ':00' ,'2017-01-' + (SELECT CONVERT(varchar(30), @day)))
		INSERT INTO timeslot VALUES (4, (SELECT CONVERT(varchar(30), @hour)) + ':00 -' + (SELECT CONVERT(varchar(30), @endOfHour)) + ':00' ,'2017-01-' + (SELECT CONVERT(varchar(30), @day)))

	
		SET @hour = @hour + 1
		SET @endOfHour = @hour + 1
	END
	
	SET @day = @day + 1
	
		IF @day = 7 
			SET @day = @day + 1

		ELSE IF @day = 14
			SET @day = @day + 1

		ELSE IF @day = 21
			SET @day = @day + 1

		ELSE IF @day = 28
			SET @day = @day + 1
END



SELECT * FROM appointment WHERE seen = 'Completed'

SELECT * FROM appointment WHERE instructor_username='brad' AND seen != 'Paid' AND  seen != 'Paid'

SELECT * FROM appointment app, type tp WHERE seen = 'Completed' AND app.competence = tp.name AND id = '6'

SELECT id, time, date, client_username FROM appointment WHERE time= '7:00-8:00' AND date = '2017-01-2  MONDAY' AND instructor_username= 'Brad'


SELECT * FROM appointment WHERE seen = 'Completed'
/* 
SELECT * FROM type
SELECT * FROM document
SELECT * FROM appointment 
SELECT * FROM timeslot
SELECT * FROM cars

SELECT * FROM instructor
SELECT * FROM admin
SELECT * FROM clients 

SELECT * FROM workingHours 

SELECT * FROM workingHours WHERE instructor_username = 'bradp' AND dayOfWeek = 'MONDAY - 13 Hrs' AND car = 'V4FFER - Toyota'

SELECT * FROM appointment WHERE instructor_username='Max'



*/
